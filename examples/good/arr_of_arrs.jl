int main() {
    int[][] arr = new int[][5];

    int i = 0;
    while (i < arr.length) {
        arr[i] = new int[i];
        int[] sub = arr[i];

        int j = 0;
        while (j < sub.length) {
            sub[j] = i;
            j++;
        }

        i++;
    }

    for (int[] sub : arr)
        for (int elem : sub)
            printInt(elem);

    return 0;
}