int main() {
    double x = 1.0, y = 2.0;
    printBool(x == x);
    printBool(x == y);
    printBool(x != x);
    printBool(x != y);
    printBool(x < x);
    printBool(x < y);
    printBool(y < x);
    printBool(x <= x);
    printBool(x <= y);
    printBool(y <= x);
    printBool(x > x);
    printBool(x > y);
    printBool(y > x);
    printBool(x >= x);
    printBool(x >= y);
    printBool(y >= x);
    return 0;
}

void printBool(boolean b) {
    if (b) printString("true");
    else printString("false");
}