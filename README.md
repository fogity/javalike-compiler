# Javalike Compiler

This project is a compiler for a loosely specified Java-like language
(referred to as javalike).
It was a project done during a compiler construction course.

The main work consists of a type checker and backends for llvm and 64 bit x86.

## The Language

The javalike language is specified syntactically in the Javalike.cf grammar file
and semantically by the compiler and the examples.

The language contains functions, integers, doubles, booleans and arrays but not
much more.
It has a couple of built in functions including the ability to print strings.

## The Examples

The examples folder contains a number of examples of incorrect and correct
programs.
The correct ones also have a corresponding file describing the expected output.

## The Runtime

The lib folder contains the runtime defining the built-in functions.
It needs to be compiled and available when running the compiler.

## The Code

The compiler is written in Haskell and is mainly divided into the frontend
(which handles type checking) and the backend (code generation).
The backend is in turn divided into a normalization step and code generation
for the two targets (llvm and x86).
The x86 part also contains optimization steps (register allocation and
peephole optimization).

BNFC is used to generate the lexer and parser for the javalike and the
generated AST is used in both the frontend and backend.
A grammar is also used to generate an AST for x86 for code generation only.

Several advanced Haskell extensions are used, most notably GADTs and
TypeFamilies.
Monad transformers and lenses are used in most of the code except for the x86
code generator that uses extensible effects.

## Compiling the Compiler

The compiler can be built using make with the following dependencies:

* stack
* bnfc
* llvm
* gcc (or an llvm alias)

## Running the Compiler

The compiler (jlc) is run from the command line with a list of javalike files
to compile.
The following flags are supported:

* __-b backend__ is used to specify what backend to use, either _llvm_ or _x86_.
* __-l dir__ is used to specify the directory containing the runtime.

The default backend is _llvm_ and the default runtime folder is _lib_ in the
working directory.

To run the compiler the following dependencies are needed:

* llvm (if using the _llvm_ backend)
* nasm (if using the _x86_ backend)
* gcc (or an llvm alias)