# Build the compiler.
jlc: src/BNFC lib/runtime.bc lib/runtime.o
	stack build

# Build the bnfc generated files.
src/BNFC: $(wildcard src/*.cf)
	for grammar in $^; do \
		bnfc --haskell-gadt -p BNFC -o src $$grammar; \
	done

# Assemble the llvm runtime.
lib/runtime.bc: lib/runtime.c
	llvm-gcc -emit-llvm -c -o $@ $<

# Compile the x86 runtime.
lib/runtime.o: lib/runtime.c
	gcc -m64 -c -o $@ $<

# Install the compiler
.PHONY: install
install: jlc
	stack install

# Clean the build files.
.PHONY: clean
clean:
	-rm -rf src/BNFC .stack-work *.cabal lib/runtime.bc lib/runtime.o