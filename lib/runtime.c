#include <stdio.h>
#include <stdlib.h>

void printInt(int x) {
    printf("%d\n", x);
}

void printDouble(double x) {
    printf("%.1f\n", x);
}

void printString(char* s) {
    puts(s);
}

int readInt() {
    int x;
    scanf("%d\n", &x);
    return x;
}

double readDouble() {
    double x;
    scanf("%lf\n", &x);
    return x;
}

char* new(int size) {
    return (char*) calloc(1, size);
}