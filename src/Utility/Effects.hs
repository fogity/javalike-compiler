module Utility.Effects where

import Control.Eff
import Control.Eff.State.Lazy

import Data.Map

-- | A map containing named counters.
type CMap = Map String Integer

-- | Returns the next number from a named counter.
new :: Member (State CMap) r => String -> Eff r Integer
new a = do
    num <- findWithDefault 0 a <$> get
    modify $ insert a (num + 1)
    return num

-- | Returns a string appended with a fresh number from a named counter.
-- First argument is the string to prepend, second argument is the counter name.
newPrefix :: Member (State CMap) r => String -> String -> Eff r String
newPrefix pre a = (pre ++) . show <$> new a