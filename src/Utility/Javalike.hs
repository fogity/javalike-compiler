module Utility.Javalike where

import BNFC.AbsJavalike

-- | Rewrites a multidimensional new expression into a block containing only lower dimensional new expressions.
-- First argument is the name to store the new array into, should be unique.
-- Forth and fifth arguments are names to use for temporary variables, should be unique.
nestedNew :: Ident -> Type -> [Idx] -> Ident -> Ident -> Blk
nestedNew name t (Index expr : idxs) v1 v2 = Block
    [ Decl oat [Init name . Typed oat $ New iat [Index expr]]
    , Decl Int [Init v1 expr, NoInit v2]
    , While (Typed Bool $ ERel idx LTH len) . BStmt $ Block
        [ Ass (LTyped iat $ Indexed name [Index idx]) . Typed iat $ New t idxs
        , Incr v2
        ]
    ]
    where
        iat = array t $ length idxs
        oat = Array iat
        len = Typed Int . LExpr . LTyped Int $ Var v1
        idx = Typed Int . LExpr . LTyped Int $ Var v2

-- | Rewrites a for loop as a while loop.
-- Fifth to seventh arguments are names to use for temporary variables, should be unique.
rewriteFor :: Type -> Ident -> Expr -> Stmt -> Ident -> Ident -> Ident -> Blk
rewriteFor t name expr stmt v1 v2 v3 = Block
    [ Decl at [Init v1 expr]
    , Decl Int [Init v2 . Typed Int $ Length arr, NoInit v3]
    , While (Typed Bool $ ERel idx LTH len) . BStmt $ Block
        [ Decl t [Init name . Typed t . LExpr . LTyped t $ Indexed v1 [Index idx]]
        , stmt
        , Incr v3
        ]
    ]
    where
        at  = Array t
        arr = Typed at  . LExpr . LTyped at  $ Var v1
        len = Typed Int . LExpr . LTyped Int $ Var v2
        idx = Typed Int . LExpr . LTyped Int $ Var v3

-- | Extracts the element type from an array type.
element :: Type -> Type
element (Array t) = element t
element t         = t

-- | Calculates the dimension of a type.
-- Scalars have dimension 0.
dim :: Type -> Int
dim (Array t) = dim t + 1
dim _         = 0

-- | Generates an array type with the given dimension and contained type.
array :: Type -> Int -> Type
array t 0 = t
array t n = Array . array t $ n - 1

-- | Determines if a type is an array type.
isArray :: Type -> Bool
isArray (Array _) = True
isArray _         = False

-- | Extracts the tagged type of an expression.
typeOf :: Expr -> Type
typeOf (Typed t _) = t
typeOf t           = error $ "INTERNAL ERROR: type checker should have tagged " ++ show t

-- | Extracts the tagged type of a left hand side expression.
typeOfL :: LExpr -> Type
typeOfL (LTyped t _) = t
typeOfL t            = error $ "INTERNAL ERROR: type checker should have tagged " ++ show t

-- | Tries to determines whether a piece of code is guaranteed to return.
doesReturn :: Tree t -> Bool
doesReturn = \case
    -- Blk
    Block stmts -> any doesReturn stmts
    -- Stmt
    Empty                     -> False
    BStmt blk                 -> doesReturn blk
    Decl _ _                  -> False
    Ass _ _                   -> False
    Incr _                    -> False
    Decr _                    -> False
    Ret _                     -> True
    VRet                      -> True
    Cond     expr stmt        -> isLiteralTrue expr && doesReturn stmt
    CondElse expr stmtT stmtF -> (isLiteralFalse expr || doesReturn stmtT) && (isLiteralTrue expr || doesReturn stmtF)
    While    expr stmt        -> isLiteralTrue expr && doesReturn stmt
    SExp _                    -> False
    For _ _ _ _               -> False
    -- Otherwise
    t -> error $ "INTERNAL ERROR: return checking does not handle: " ++ show t

-- | Determines if an expression is the literal true.
isLiteralTrue :: Expr -> Bool
isLiteralTrue ELitTrue           = True
isLiteralTrue (Typed _ ELitTrue) = True
isLiteralTrue _                  = False

-- | Determines if an expression is the literal false.
isLiteralFalse :: Expr -> Bool
isLiteralFalse ELitFalse           = True
isLiteralFalse (Typed _ ELitFalse) = True
isLiteralFalse _                   = False