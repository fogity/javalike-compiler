module Utility.Functions where

import Data.Char
import Data.Maybe

import Numeric

-- | A safe version of 'head'.
head' :: [a] -> Maybe a
head' [] = Nothing
head' (a : _) = Just a

-- | A function for extracting the first availiable value from some structure within a list.
findDeep :: (a -> Maybe b) -> [a] -> Maybe b
findDeep f = head' . catMaybes . map f

-- | Returns all elements preceeding and including the first element to match the predicate.
takeUntil' :: (a -> Bool) -> [a] -> [a]
takeUntil' _ [] = []
takeUntil' p (a : as)
    | p a       = [a]
    | otherwise = a : takeUntil' p as

-- | Replaces non-printable characters with their escape sequences.
-- First argument determines if the escape sequences should have the form \x## or \##.
escape :: Bool -> String -> String
escape _ "" = ""
escape prefix (c:s)
    | c `elem` "\"\\" || isControl c = esc ++ escape prefix s
    | otherwise                      = c    : escape prefix s
    where
        esc = '\\' : if prefix then 'x' : hex' else hex'
        hex' = case length hex of
            1 -> '0' : hex
            2 -> hex
            _ -> error $ "INTERNAL ERROR: compiler could not handle the string literal character " ++ show c
        hex = showHex (ord c) ""