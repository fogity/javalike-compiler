import Data.Char

import System.Environment

import JLC.Run

-- | The main function processes each input file in turn.
main :: IO ()
main = do
    (lib, backend, files) <- processArgs <$> getArgs
    mapM_ (processFile lib backend) files

-- | Parses the command line arguments.
processArgs :: [String] -> (FilePath, Backend, [FilePath])
processArgs [] = ("lib", LLVM, [])
processArgs ["-b"] = error "USER ERROR: must specifiy backend when using -b flag"
processArgs ["-l"] = error "USER ERROR: must specifiy path when using -l flag"
processArgs ("-b" : name : args) = (lib, , files) $ case map toLower name of
    "llvm"   -> LLVM
    "x86"    -> X86
    "x86_64" -> X86
    _        -> error $ "INTERNAL ERROR: backend not supported " ++ name
    where (lib, _, files) = processArgs args
processArgs ("-l" : path : args) = (path, backend, files)
    where (_, backend, files) = processArgs args
processArgs (file : args) = (lib, backend, file : files)
    where (lib, backend, files) = processArgs args