module Backend.LLVM.Compile (compile) where

import Control.Monad.State
import Control.Monad.Writer

import Data.List hiding (insert)
import Data.Map hiding (filter, foldl)

import Lens.Micro.Platform hiding (assign)

import BNFC.AbsJavalike

import Utility.Functions
import Utility.Javalike

-- | The state used during compilation.
data CS = CS
    { _strMap  :: Map String Ident -- ^ A map to get the name of a string literal.
    , _params  :: [Ident]          -- ^ A list of names that correspond to parameters.
    , _lblNum  :: Int              -- ^ The next number to be used to name a label.
    , _varNum  :: Int              -- ^ The next number to be used to name a temporary variable.
    , _dType   :: Type             -- ^ The type of the current variable declaration.
    , _currLbl :: Ident            -- ^ The name of the current label.
    }

-- Make lenses for the compilation state.
makeLenses ''CS

-- | Compiles a program.
-- Requires the input to be normalized.
compile :: Prog -> String
compile = execWriter . flip evalStateT emptyCS . comp

-- | The default state for compilation.
emptyCS :: CS
emptyCS = CS {_strMap = empty, _params = [], _lblNum = 0, _varNum = 0, _dType = Void, _currLbl = Ident ""}

-- | Represents a value returned by an expression.
data Value = Lit String | V Ident | None

-- | The monad used for compilation. Holds the state and writes the output code.
type C = StateT CS (Writer String)

-- | Used to specify the return type of a compilation step based on the input.
type family Ret a where
    Ret LExpr = Value         -- Left hand side expressions return their value.
    Ret Expr  = (Type, Value) -- Expressions return their type and their value.
    Ret _     = ()            -- All others do not return anything.

-- | Compiles a part of the program.
comp :: Tree t -> C (Ret (Tree t))
comp = \case
    -- Prog
    Program defs -> do
        -- Extracts and compiles all string literals.
        compStringLiterals $ Program defs
        endLine
        -- Compiles the built in declarations.
        compBuiltIns
        endLine
        -- Compiles each definition.
        mapM_ comp defs
    -- TopDef
    FnDef t name args blk -> do
        params .= fmap (\(Argument _ n) -> n) args
        lblNum .= 0
        varNum .= 0
        tell "define " >> comp t >> space >> global name >> parList (fmap comp args) >> tell " {\n"
        label =<< newLabel
        comp blk
        tell "}\n\n"
    -- Arg
    Argument t name -> comp t >> space >> local name
    -- Blk
    Block stmts -> mapM_ comp stmts
    -- Stmt
    Empty -> return ()
    BStmt blk -> comp blk
    Decl t items -> do
        dType .= t
        mapM_ comp items
    Ass (LTyped t ex) expr -> do
        (_, res) <- comp expr
        case ex of
            Var name -> store t res name
            Indexed name idxs -> do
                t1 <- newVar
                load (array t $ length idxs) name t1
                ptr <- getElementPointer t t1 idxs
                store t res ptr
            _ -> error $ "INTERNAL ERROR: llvm compiler can not handle the left hand side expression " ++ show ex
    -- Increments and decrements are replaced by assignments and addition or subtraction.
    Incr name -> comp . Ass (LTyped Int $ Var name) . Typed Int
        $ EAdd (Typed Int . LExpr . LTyped Int $ Var name) Plus (Typed Int $ ELitInt 1)
    Decr name -> comp . Ass (LTyped Int $ Var name) . Typed Int
        $ EAdd (Typed Int . LExpr . LTyped Int $ Var name) Minus (Typed Int $ ELitInt 1)
    Ret expr -> do
        (t, res) <- comp expr
        inst Nothing "ret" [comp t >> space >> value res]
    VRet -> inst Nothing "ret" [comp Void]
    Cond expr stmt -> do
        start <- newLabel
        end   <- newLabel
        (t, res) <- comp expr
        branch t res start end
        label start
        comp stmt
        unless (doesReturn stmt) $ jump end
        label end
    CondElse expr stmtT stmtF -> do
        let retT = doesReturn stmtT
            retF = doesReturn stmtF
        true  <- newLabel
        false <- newLabel
        end   <- newLabel
        (t, res) <- comp expr
        branch t res true false
        label true
        comp stmtT
        unless retT $ jump end
        label false
        comp stmtF
        unless retF $ jump end
        unless (retT && retF) $ label end
    While expr stmt -> do
        check <- newLabel
        start <- newLabel
        end   <- newLabel
        jump check
        label check
        (t, res) <- comp expr
        branch t res start end
        label start
        comp stmt
        unless (doesReturn stmt) $ jump check
        label end
    SExp expr -> void $ comp expr
    For t name expr stmt -> rewriteFor t name expr stmt <$> newVar <*> newVar <*> newVar >>= comp
    -- Item
    NoInit name -> do
        t <- use dType
        alloc t name
        store t (defaultValue t) name
    Init name expr -> do
        t <- use dType
        alloc t name
        (_, res) <- comp expr
        store t res name
    -- Type
    Int     -> tell "i32"
    Doub    -> tell "double"
    Bool    -> tell "i1"
    Void    -> tell "void"
    Array t -> tell "{i32, [0 x " >> comp t >> tell "]}*"
    Str     -> tell "i8*"
    -- LExpr
    LTyped t ex -> case ex of
        Var name -> do
            ps <- use params
            -- Parameters need to be treated differently to variables as they are not pointers.
            if name `elem` ps
                then return $ V name
                else do
                    t1 <- newVar
                    load t name t1
                    return $ V t1
        Indexed name idxs -> do
            t1 <- newVar
            t2 <- newVar
            load (array t $ length idxs) name t1
            ptr <- getElementPointer t t1 idxs
            load t ptr t2
            return $ V t2
        lexpr -> error $ "INTERNAL ERROR: llvm compiler can not handle the expression " ++ show lexpr
    -- Expr
    Typed t ex -> (t,) <$> case ex of
        ELitInt  i -> return . Lit $ show i
        ELitDoub d -> return . Lit $ show d
        ELitTrue   -> return $ Lit "true"
        ELitFalse  -> return $ Lit "false"
        EApp name exprs -> do
            args <- fmap (\(t', res) -> comp t' >> space >> value res) <$> mapM comp exprs
            t1 <- newVar
            let ass = if t == Void then Nothing else Just t1
            inst ass "call" [comp t >> space >> global name >> parList args]
            return $ if t == Void then None else V t1
        EString s -> use (strMap . at s) >>= \case
            Just name -> do
                let n = show $ length s + 1
                t1 <- newVar
                tab
                local t1
                tell $ " = getelementptr [" ++ n ++ " x i8], [" ++ n ++ " x i8]* "
                global name
                tell $ ", i32 0, i32 0\n"
                return $ V t1
            Nothing -> error $ "INTERNAL ERROR: llvm compiler failed on the literal string "
        Length expr -> do
            (t', arr) <- comp expr
            t1 <- newVar
            ptr <- getLengthPointer t' arr
            load t ptr t1
            return $ V t1
        LExpr lexpr -> comp lexpr
        Neg expr -> do
            (_, res) <- comp expr
            t1 <- newVar
            oper t (fromOp t Minus) (defaultValue t) res t1
            return $ V t1
        Not expr -> do
            (_, res) <- comp expr
            t1 <- newVar
            oper t "xor" (Lit "true") res t1
            return $ V t1
        EMul exprL op exprR -> oper' op exprL exprR
        EAdd exprL op exprR -> oper' op exprL exprR
        ERel exprL op exprR -> oper' op exprL exprR
        EAnd exprL exprR -> do
            start <- newLabel
            end   <- newLabel
            (_, resL) <- comp exprL
            left <- use currLbl
            branch t resL start end
            label start
            (_, resR) <- comp exprR
            right <- use currLbl
            jump end
            label end
            t1 <- newVar
            phi t [(Lit "false", left), (resR, right)] t1
            return $ V t1
        EOr exprL exprR -> do
            start <- newLabel
            end   <- newLabel
            (_, resL) <- comp exprL
            left <- use currLbl
            branch t resL end start
            label start
            (_, resR) <- comp exprR
            right <- use currLbl
            jump end
            label end
            t1 <- newVar
            phi t [(Lit "true", left), (resR, right)] t1
            return $ V t1
        New t' idxs -> compNew t' idxs
        expr -> error $ "INTERNAL ERROR: llvm compiler can not handle the expression " ++ show expr
    -- Otherwise
    t -> error $ "INTERNAL ERROR: llvm compiler can not handle " ++ show t

-- | Prints an array type struct not as a pointer.
unPtr :: Type -> C ()
unPtr (Array t) = tell "{i32, [0 x " >> comp t >> tell "]}"
unPtr _         = error "INTERNAL ERROR: trying to un-pointer non-array type"

-- | Allocates and zeros a new array with the given dimensions.
compNew :: Type -> [Idx] -> C Value
compNew _ [] = error "INTERNAL ERROR: zero-dimensional indexing not allowed"
compNew t [Index expr] = do
    t1 <- newVar
    t2 <- newVar
    t3 <- newVar
    t4 <- newVar
    (_, num) <- comp expr
    eSize <- sizeOf t
    oper Int "mul" num eSize t1
    iSize <- sizeOf Int
    oper Int "add" (V t1) iSize t2
    local t3 >> tell " = call i8* @new(i32 " >> local t2 >> tell ")\n"
    local t4 >> tell " = bitcast i8* " >> local t3 >> tell " to " >> comp (Array t) >> endLine
    ptr <- getLengthPointer (Array t) $ V t4
    store Int num ptr
    return $ V t4
compNew t idxs = do
    t1 <- newVar
    t2 <- newVar
    nestedNew t1 t idxs <$> newVar <*> newVar >>= comp
    load (array t $ length idxs) t1 t2
    return $ V t2

-- | Computes the size of a given type.
sizeOf :: Type -> C Value
sizeOf t = do
    t1 <- newVar
    t2 <- newVar
    local t1 >> tell " = getelementptr " >> comp t >> tell ", " >> comp t >> tell "* null, i32 1\n"
    local t2 >> tell " = ptrtoint " >> comp t >> tell "* " >> local t1 >> tell " to i32\n"
    return $ V t2

-- | Returns the pointer to the length of the given array.
getLengthPointer :: Type -> Value -> C Ident
getLengthPointer t arr = do
    t1 <- newVar
    local t1 >> tell " = getelementptr " >> unPtr t >> tell ", " >> comp t >> space
        >> value arr >> tell ", i32 0, i32 0\n"
    return t1

-- | Returns the pointer to an element given by the indicies in the given array.
getElementPointer :: Type -> Ident -> [Idx] -> C Ident
getElementPointer _ _ [] = error "INTERNAL ERROR: zero-dimensional indexing not allowed"
getElementPointer t arr [Index expr] = let t' = Array t in do
    (_, num) <- comp expr
    t1 <- newVar
    local t1 >> tell " = getelementptr " >> unPtr t' >> tell ", " >> comp t' >> space
        >> local arr >> tell ", i32 0, i32 1, i32 " >> value num >> endLine
    return t1
getElementPointer t arr (idx : idxs) = let t' = array t $ length idxs in do
    t1 <- newVar
    ptr <- getElementPointer t' arr [idx]
    load t' ptr t1
    getElementPointer t t1 idxs

-- | Extracts, compiles and stores the names of each string literal in the program.
compStringLiterals :: Prog -> C ()
compStringLiterals prog = zipWithM_ compStrLit [1..] strs
    where
        -- Identical strings are not duplicated.
        strs = nub $ extract prog
        -- | Extracts each string literal.
        extract :: Tree t -> [String]
        extract (EString s) = [s]
        extract t           = composOpMonoid extract t
        -- | Stores and compiles a string literal.
        compStrLit :: Int -> String -> C ()
        compStrLit n s = do
            strMap %= insert s name
            global name
            tell $ " = internal constant [" ++ show num ++ " x i8] c\"" ++ escape False s ++ "\\00\"\n"
            where
                name = Ident $ 's' : show n
                num  = length s + 1

-- | The built in function declarations are just put into the code verbatim.
compBuiltIns :: C ()
compBuiltIns = tell $ unlines
    [ "declare void @printInt(i32)"
    , "declare void @printDouble(double)"
    , "declare void @printString(i8*)"
    , "declare i32 @readInt()"
    , "declare double @readDouble()"
    , "declare i8* @new(i32)"
    ]

-- | Returns a new label.
newLabel :: C Ident
newLabel = do
    lbl <- Ident . ('l':) . show <$> use lblNum
    lblNum %= (+1)
    return lbl

-- | Returns a new temporary variable.
newVar :: C Ident
newVar = do
    var <- Ident . ('t':) . show <$> use varNum
    varNum %= (+1)
    return var

-- | Returns the default value based on the given type.
defaultValue :: Type -> Value
defaultValue Int  = Lit "0"
defaultValue Doub = Lit "0.0"
defaultValue Bool = Lit "false"
defaultValue t    = error $ "INTERNAL ERROR: no default value for " ++ show t

-- | Compiles a label and sets it as the current label.
label :: Ident -> C ()
label (Ident name) = do
    currLbl .= Ident name
    tell name >> tell ":"

-- | The unconditional branch instruction.
jump :: Ident -> C ()
jump lbl = inst Nothing "br" [tell "label " >> local lbl]

-- | The conditional branch instruction.
branch :: Type -> Value -> Ident -> Ident -> C ()
branch t res lbl1 lbl2 = inst Nothing "br"
    [ comp t >> space >> value res
    , tell "label " >> local lbl1
    , tell "label " >> local lbl2
    ]

-- | The phi instruction used to merge values from multiple preceeding basic blocks into one variable.
phi :: Type -> [(Value, Ident)] -> Ident -> C ()
phi _ []     _   = error "INTERNAL ERROR: empty list given to phi in llvm compiler"
phi t (b:bs) var = inst (Just var) "phi" $ (comp t >> space >> format b) : fmap format bs
    where format (v, l) = tell "[ " >> value v >> tell ", " >> local l >> tell " ]"

-- | The variable allocation instruction.
alloc :: Type -> Ident -> C ()
alloc t name = inst (Just name) "alloca" [comp t]

-- | The variable assignment instruction.
store :: Type -> Value -> Ident -> C ()
store t res name = inst Nothing "store" [comp t >> space >> value res, comp t >> tell "* " >> local name]

-- | The variable fetching instruction.
load :: Type -> Ident -> Ident -> C ()
load t name var = inst (Just var) "load" [comp t, comp t >> tell "* " >> local name]

-- | Generalization of each binary operator instruction.
oper :: Type -> String -> Value -> Value -> Ident -> C ()
oper t op val1 val2 var = inst (Just var) op [comp t >> space >> value val1, value val2]

-- | A variant of 'oper' that does more work.
oper' :: Tree t -> Expr -> Expr -> C Value
oper' op expr1 expr2 = do
    (t, res1) <- comp expr1
    (_, res2) <- comp expr2
    t1 <- newVar
    oper t (fromOp t op) res1 res2 t1
    return $ V t1

-- | Returns the corresponding instruction based on a javalike operation and a type.
fromOp :: Type -> Tree t -> String
fromOp t = \case
    -- AddOp
    Plus  -> if t == Doub then "fadd" else "add"
    Minus -> if t == Doub then "fsub" else "sub"
    -- MulOp
    Times -> if t == Doub then "fmul" else "mul"
    Div   -> if t == Doub then "fdiv" else "sdiv"
    Mod   -> "srem"
    -- RelOp
    LTH -> if t == Doub then "fcmp olt" else "icmp slt"
    LE  -> if t == Doub then "fcmp ole" else "icmp sle"
    GTH -> if t == Doub then "fcmp ogt" else "icmp sgt"
    GE  -> if t == Doub then "fcmp oge" else "icmp sge"
    EQU -> if t == Doub then "fcmp oeq" else "icmp eq"
    NE  -> if t == Doub then "fcmp one" else "icmp ne"
    -- Otherwise
    op -> error $ "INTERNAL ERROR: unknown operation " ++ show op

-- | Compiles an instruction.
-- Takes a possible name to assign to, the instruction name, and a list of elements to compile.
inst :: Maybe Ident -> String -> [C ()] -> C ()
inst ass name args = tab >> assign >> tell name >> space >> commaList args >> endLine
    where
        assign = case ass of
            Just var -> local var >> tell " = "
            Nothing  -> return ()

-- | Compiles a value.
value :: Value -> C ()
value (Lit s) = tell s
value (V n)   = local n
value None    = error $ "INTERNAL ERROR: llvm compiler tries to use a non-existant value"

-- | Adds a tab character.
tab :: C ()
tab = tell "\t"

-- | Adds a space.
space :: C ()
space = tell " "

-- | Adds a line end.
endLine :: C ()
endLine = tell "\n"

-- | Compiles a global variable name.
global :: Ident -> C ()
global (Ident name) = tell $ '@' : name

-- | Compiles a local variable name.
local :: Ident -> C ()
local (Ident name) = tell $ '%' : name

-- | Compiles a list of elements into a parantesized comma-separated list.
parList :: [C ()] -> C ()
parList ts = tell "(" >> commaList ts >> tell ")"

-- | Compiles a list of elements into a comma-separated list.
commaList :: [C ()] -> C ()
commaList [] = return ()
commaList ts = foldl1 (\a b -> a >> tell ", " >> b) ts