module Backend.Normalize (normalize) where

import Prelude as P hiding (truncate)

import Control.Monad.State

import Data.Map as M

import Lens.Micro.Platform

import BNFC.AbsJavalike

import Utility.Functions
import Utility.Javalike

-- | The type of a scope in the variable renaming environment.
type Scope = Map Ident Ident

-- | The type of the variable renaming environment.
type Env = [Scope]

-- | The state used for renaming variables.
data RS = RS
    { _env    :: Env -- ^ The environment mapping old names to new.
    , _argNum :: Int -- ^ The next number to be used to name a parameter.
    , _varNum :: Int -- ^ The next number to be used to name a variable.
    }

-- Makes lenses for the renaming state.
makeLenses ''RS

-- | Normalizes a program.
-- This includes renaming variables, promoting parameters, simplifying control flows and inserting returns.
normalize :: Prog -> Prog
normalize = insertReturn . promoteParameters . truncate . flatten . flip evalState emptyRS . rename

-- | Promotes parameters into variables.
promoteParameters :: Tree t -> Tree t
promoteParameters = \case
    -- Prog
    Program defs -> Program $ fmap promoteParameters defs
    -- TopDef
    FnDef t name args blk -> FnDef t name args blk'
        where
            -- New variable declarations are added at the start of the function.
            blk' = Block $ decls ++ stmts
            -- Declarations are initiated with the value of the parameter.
            decls = fmap (\(t', old, new) -> Decl t' [Init new . Typed t' . LExpr . LTyped t' $ Var old]) reps
            -- The new variables are named "ppX" where "X" is the parameter number.
            reps = fmap (\(Argument t' (Ident n)) -> (t', Ident n, Ident $ 'p' : n)) args
            -- Each occurance of a promoted parameter in the function body is replaced by the new variable.
            Block stmts = P.foldl (\b (_, old, new) -> replace old new b) blk reps
            -- | Replaces each occurance of a variable with another.
            replace :: Ident -> Ident -> Tree t -> Tree t
            replace (Ident old) (Ident new) = \case
                Ident n -> Ident $ if n == old then new else n
                tree    -> composOp (replace (Ident old) (Ident new)) tree
    -- Otherwise
    t -> error $ "INTERNAL ERROR: promoting parameters does not handle " ++ show t

-- | The monad used for renaming.
type R = State RS

-- | The empty state for renaming.
emptyRS :: RS
emptyRS = RS {_env = [], _argNum = 0, _varNum = 0}

-- | Renames each parameter and variable in the given tree.
rename :: Tree t -> R (Tree t)
rename = \case
    -- Prog
    Program defs -> Program <$> mapM rename defs
    -- TopDef
    FnDef t name args blk -> do
        env .= [empty]
        argNum .= 0
        varNum .= 0
        FnDef t name <$> mapM rename args <*> rename blk
    -- Arg
    Argument t name -> Argument t <$> addArg name
    -- Blk
    Block stmts -> Block <$> inNewScope (mapM rename stmts)
    -- Stmt
    Empty -> return Empty
    BStmt blk -> BStmt <$> rename blk
    Decl t items -> Decl t <$> mapM rename items
    Ass lexpr expr -> Ass <$> rename lexpr <*> rename expr
    Incr name -> Incr <$> getName name
    Decr name -> Decr <$> getName name
    Ret expr -> Ret <$> rename expr
    VRet -> return VRet
    Cond expr stmt -> Cond <$> rename expr <*> inNewScope (rename stmt)
    CondElse expr stmtT stmtF -> CondElse <$> rename expr <*> inNewScope (rename stmtT) <*> inNewScope (rename stmtF)
    While expr stmt -> While <$> rename expr <*> inNewScope (rename stmt)
    SExp expr -> SExp <$> rename expr
    For t name expr stmt -> do
        (var, stmt') <- inNewScope $ (,) <$> addVar name <*> rename stmt
        For t var <$> rename expr <*> pure stmt'
    -- Item
    NoInit name -> NoInit <$> addVar name
    Init name expr -> rename expr >>= \expr' -> flip Init expr' <$> addVar name
    -- Idx
    Index expr -> Index <$> rename expr
    -- LExpr
    Var name -> Var <$> getName name
    Indexed name idxs -> Indexed <$> getName name <*> mapM rename idxs
    LTyped t lexpr -> LTyped t <$> rename lexpr
    -- Expr
    ELitInt i -> return $ ELitInt i
    ELitDoub d -> return $ ELitDoub d
    ELitTrue -> return ELitTrue
    ELitFalse -> return ELitFalse
    EApp name exprs -> EApp name <$> mapM rename exprs
    EString s -> return $ EString s
    Length expr -> Length <$> rename expr
    LExpr lexpr -> LExpr <$> rename lexpr
    Neg expr -> Neg <$> rename expr
    Not expr -> Not <$> rename expr
    EMul exprL op exprR -> EMul <$> rename exprL <*> pure op <*> rename exprR
    EAdd exprL op exprR -> EAdd <$> rename exprL <*> pure op <*> rename exprR
    ERel exprL op exprR -> ERel <$> rename exprL <*> pure op <*> rename exprR
    EAnd exprL exprR -> EAnd <$> rename exprL <*> rename exprR
    EOr exprL exprR -> EOr <$> rename exprL <*> rename exprR
    New t idxs -> New t <$> mapM rename idxs
    Typed t expr -> Typed t <$> rename expr
    -- Otherwise
    t -> error $ "INTERNAL ERROR: variable renaming does not handle " ++ show t

-- | Sets and returns a new name for a parameter.
addArg :: Ident -> R Ident
addArg name = do
    new <- Ident . ('p':) . show <$> use argNum
    env . _head %= insert name new
    argNum %= (+1)
    return new

-- | Sets and returns a new name for a variable.
addVar :: Ident -> R Ident
addVar name = do
    new <- Ident . ('v':) . show <$> use varNum
    env . _head %= insert name new
    varNum %= (+1)
    return new

-- | Gets the new name of a variable.
getName :: Ident -> R Ident
getName name = findDeep (M.lookup name) <$> use env >>= \case
    Just new -> return new
    Nothing  -> error "INTERNAL ERROR: type checker did not catch undeclared variable"

-- | Runs a renaming action within a new scope.
inNewScope :: R a -> R a
inNewScope action = do
    env %= (empty:)
    res <- action
    env %= tail
    return res

-- | Used to specify the return type based on input type in 'flatten'.
type family F a where
    F Prog   = Prog   -- Programs are turned into equivalent programs.
    F TopDef = TopDef -- Definitions are turned into equivalent definitions.
    F Blk    = [Stmt] -- Blocks are turned into lists of statements.
    F Stmt   = [Stmt] -- A statement is turned into a list of statements.

-- | Eliminates redundant blocks and control structures.
-- Requires variables to have been renamed.
flatten :: Tree t -> F (Tree t)
flatten = \case
    -- Prog
    Program defs -> Program $ fmap flatten defs
    -- TopDef
    FnDef t name args blk -> FnDef t name args . Block $ flatten blk
    -- Blk
    Block stmts -> concatMap flatten stmts
    -- Stmt
    Empty -> []
    BStmt blk -> flatten blk
    Decl t items -> [Decl t items]
    Ass name expr -> [Ass name expr]
    Incr name -> [Incr name]
    Decr name -> [Decr name]
    Ret expr -> [Ret expr]
    VRet -> [VRet]
    Cond (Typed _ ELitTrue) stmt -> flatten stmt
    Cond (Typed _ ELitFalse) _ -> []
    Cond expr stmt -> case flatten stmt of
        []    -> []
        stmts -> [Cond expr $ toStmt stmts]
    CondElse (Typed _ ELitTrue) stmtT _ -> flatten stmtT
    CondElse (Typed _ ELitFalse) _ stmtF -> flatten stmtF
    CondElse expr stmtT stmtF -> case (flatten stmtT, flatten stmtF) of
        ([]    , []    ) -> []
        (stmts , []    ) -> [Cond expr $ toStmt stmts]
        ([]    , stmts ) -> [Cond (Typed Bool $ Not expr) $ toStmt stmts]
        (stmtT', stmtF') -> [CondElse expr (toStmt stmtT') (toStmt stmtF')]
    While (Typed _ ELitFalse) _ -> []
    While expr stmt -> let stmts = flatten stmt in if any doesReturn stmts
        then if expr == Typed Bool ELitTrue
            then stmts
            else [Cond expr $ toStmt stmts]
        else [While expr $ toStmt stmts]
    SExp expr -> [SExp expr]
    For t name expr stmt -> case flatten stmt of
        []    -> []
        stmts -> [For t name expr $ toStmt stmts]
    -- Otherwise
    t -> error $ "INTERNAL ERROR: flattening does not handle " ++ show t

-- | Turns a list of statements into a single statement.
toStmt :: [Stmt] -> Stmt
toStmt []     = Empty
toStmt [stmt] = stmt
toStmt stmts  = BStmt $ Block stmts

-- | Eliminates all statements following a returning statement in every block.
truncate :: Tree t -> Tree t
truncate = \case
    -- Prog
    Program defs -> Program $ fmap truncate defs
    -- TopDef
    FnDef t name args blk -> FnDef t name args $ truncate blk
    -- Blk
    Block stmts -> Block . fmap truncate $ takeUntil' doesReturn stmts
    -- Stmt
    Empty -> Empty
    BStmt blk -> BStmt $ truncate blk
    Decl t items -> Decl t items
    Ass name expr -> Ass name expr
    Incr name -> Incr name
    Decr name -> Decr name
    Ret expr -> Ret expr
    VRet -> VRet
    Cond expr stmt -> Cond expr $ truncate stmt
    CondElse expr stmtT stmtF -> CondElse expr (truncate stmtT) (truncate stmtF)
    While expr stmt -> While expr $ truncate stmt
    SExp expr -> SExp expr
    For t name expr stmt -> For t name expr $ truncate stmt
    -- Otherwise
    t -> error $ "INTERNAL ERROR: truncation does not handle " ++ show t

-- | Inserts any missing returns in void functions.
-- Requires blocks to have been truncated.
insertReturn :: Tree t -> Tree t
insertReturn = \case
    -- Prog
    Program defs -> Program $ fmap insertReturn defs
    -- TopDef
    FnDef t name args blk -> FnDef t name args $ if t == Void then insertReturn blk else blk
    -- Blk
    Block []    -> Block [VRet]
    Block stmts -> Block $ if doesReturn (last stmts) then stmts else stmts ++ [VRet]
    -- Otherwise
    t -> error $ "INTERNAL ERROR: inserting returns does not handle " ++ show t