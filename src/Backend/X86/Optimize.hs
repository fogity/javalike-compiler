module Backend.X86.Optimize (optimize) where

import BNFC.AbsX

-- | Runs a simple peephole optimization on each text segment.
optimize :: Prog -> Prog
optimize (Program decls sects) = Program decls $ fmap opt sects
    where
        opt (Text insts) = Text $ peep insts
        opt s            = s

-- | Simple peephole optimization.
peep :: [Inst] -> [Inst]
peep = \case
    []  -> []
    -- Any jump followed be the jump label can be removed.
    Jump _ lbl1 : Label lbl2 : is | lbl1 == lbl2
        -> peep $ Label lbl2 : is
    -- All unlabeled instructions after an unconditional jump can be removed.
    Jump None lbl : i : is | notLabel i
        -> peep $ Jump None lbl : is
    -- A jump that only skips an unconditional jump can be replaced with a logically inverted jump.
    Jump cond lbl1 : Jump None lbl2 : Label lbl3 : is | lbl1 == lbl3
        -> peep $ Jump (invert cond) lbl2 : Label lbl3 : is
    -- Moves to and from the same location can be removed.
    Mov d s : is | d == s
        -> peep is
    MovSD d s : is | d == s
        -> peep is
    -- Only need to keep the second of two consecutive moves to the same destination.
    Mov d1 _ : Mov d2 s : is | d1 == d2
        -> peep $ Mov d2 s : is
    MovSD d1 _ : MovSD d2 s : is | d1 == d2
        -> peep $ MovSD d2 s : is
    -- Calculations involving literals can be statically evaluated.
    Mov d1 (Literal a) : Add d2 (Literal b) : is | d1 == d2
        -> peep $ Mov d1 (Literal $ a + b) : is
    Mov d1 (Literal a) : Sub d2 (Literal b) : is | d1 == d2
        -> peep $ Mov d1 (Literal $ a - b) : is
    Mov d1 (Literal a) : IMul d2 (Literal b) : is | d1 == d2
        -> peep $ Mov d1 (Literal $ a * b) : is
    Mov d1 (Literal a) : Inc d2 : is | d1 == d2
        -> peep $ Mov d1 (Literal $ a + 1) : is
    Mov d1 (Literal a) : Dec d2 : is | d1 == d2
        -> peep $ Mov d1 (Literal $ a - 1) : is
    -- Comparisons of two literals can be statically evaluated and removed or turned into a jump.
    Mov d1 (Literal a) : CMP d2 (Literal b) : Jump cond lbl : is | d1 == d2
        -> peep $ Mov d1 (Literal a) : if holds cond a b then Jump None lbl : is else is
    -- The compiler tends to overuse virtual registers, we can remove some of these (with some code analysis).
    Mov (Virt n1) s : Mov d (Virt n2) : is | n1 == n2 && all (not . virtOccur n1) is
        -> peep $ Mov d s : is
    MovSD (VirtD n1) s : MovSD d (VirtD n2) : is | n1 == n2 && all (not . virtDOccur n1) is
        -> peep $ MovSD d s : is
    Mov (Virt n) _ : is | all (not . virtOccur n) is
        -> peep is
    MovSD (VirtD n) _ : is | all (not . virtDOccur n) is
        -> peep is
    -- Continue if no patterns are matched.
    i : is
        -> i : peep is

-- | Determines i an instruction is not a label.
notLabel :: Inst -> Bool
notLabel (Label _) = False
notLabel _         = True

-- | Logically inverts a branching condition.
invert :: Cond -> Cond
invert = \case
    E    -> NE
    NE   -> E
    L    -> GE
    LE   -> G
    G    -> LE
    GE   -> L
    A    -> BE
    AE   -> B
    B    -> AE
    BE   -> A

-- | Checks if a condition for two literals hold.
holds :: Cond -> Integer -> Integer -> Bool
holds = \case
    None -> const $ const True
    E    -> (==)
    NE   -> (/=)
    L    -> (<)
    LE   -> (<=)
    G    -> (>)
    GE   -> (>=)

-- | Checks if an x86 tree contains a specific virtual register.
virtOccur :: Integer -> Tree t -> Bool
virtOccur n = \case
    Virt n' -> n == n'
    t       -> composOpFold False (||) (virtOccur n) t

-- | Checks if an x86 tree contains a specific virtual SIMD register.
virtDOccur :: Integer -> Tree t -> Bool
virtDOccur n = \case
    VirtD n' -> n == n'
    t        -> composOpFold False (||) (virtDOccur n) t