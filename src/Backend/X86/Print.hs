module Backend.X86.Print (printX86) where

import Data.List

import BNFC.AbsX
import BNFC.PrintX

import Utility.Functions

-- | Prints the x86 AST as a string.
printX86 :: Prog -> String
printX86 (Program decls sects)
    = intercalate "\n" (fmap printTree decls)
    ++ "\n\n"
    ++ intercalate "\n\n" (fmap printSect sects)

-- | Prints a section.
printSect :: Sect -> String
printSect = \case
    Data defs  -> "section .data\n" ++ intercalate "\n" (fmap printDef defs)
    Text insts -> "section .text\n" ++ intercalate "\n" (fmap printInst insts)

-- | Prints a definition in a data section.
printDef :: Def -> String
printDef = \case
    DB (Ident lbl) s -> lbl ++ " db `" ++ escape True s ++ "\\0`"
    -- Let BNFC print other cases
    t -> printTree t

-- | Prints an instruction in a text section.
printInst :: Inst -> String
printInst = \case
    Label (Ident lbl) -> lbl ++ ":"
    Mov dest src -> "mov qword " ++ printArg dest ++ ", " ++ printArg src
    LEA dest src -> "lea " ++ printArg dest ++ ", " ++ printArg src
    MovSD dest src -> "movsd " ++ printArg dest ++ ", " ++ printArg src
    Push src -> "push " ++ printArg src
    Pop dest -> "pop " ++ printArg dest
    Inc a -> "inc qword " ++ printArg a
    Dec a -> "dec qword " ++ printArg a
    Add a b -> "add qword " ++ printArg a ++ ", " ++ printArg b
    AddSD a b -> "addsd " ++ printArg a ++ ", " ++ printArg b
    Sub a b -> "sub qword " ++ printArg a ++ ", " ++ printArg b
    SubSD a b -> "subsd " ++ printArg a ++ ", " ++ printArg b
    IMul a b -> "imul qword " ++ printArg a ++ ", " ++ printArg b
    MulSD a b -> "mulsd " ++ printArg a ++ ", " ++ printArg b
    IDiv a -> "idiv qword " ++ printArg a
    DivSD a b -> "divsd " ++ printArg a ++ ", " ++ printArg b
    CMP a b -> "cmp qword " ++ printArg a ++ ", " ++ printArg b
    COMISD a b -> "comisd " ++ printArg a ++ ", " ++ printArg b
    Jump cond (Ident lbl) -> "j" ++ printTree cond ++ " " ++ lbl
    -- Let BNFC print other cases
    t -> printTree t

-- | Prints an argument to an instruction.
printArg :: Arg -> String
printArg = \case
    Register reg -> printReg reg
    Address reg n -> if
        | n == 0    -> "[" ++ printReg reg ++ "]"
        | n <  0    -> "[" ++ printReg reg ++ show n ++ "]"
        | otherwise -> "[" ++ printReg reg ++ "+" ++ show n ++ "]"
    -- Let BNFC print other cases
    t -> printTree t

-- | Prints a register.
printReg :: Reg -> String
printReg = \case
    R n -> "r" ++ show n
    XMM n -> "xmm" ++ show n
    -- Let BNFC print other cases
    t -> printTree t