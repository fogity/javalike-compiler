module Backend.X86.Allocate (allocate) where

import Prelude as P

import Data.List as L hiding (group)
import Data.Map as M

import BNFC.AbsX

-- | Runs register allocation on each function.
allocate :: Prog -> Prog
allocate (Program defs sects) = Program defs $ fmap allocSect sects
    where
        allocSect (Text insts) = Text $ alloc insts
        allocSect s            = s

-- | Runs register allocation on the given instructions.
alloc :: [Inst] -> [Inst]
alloc insts
    -- Fixes any illegal instructions due to allocation and removes dummy instructions.
    = fix . L.filter (not . isDummy)
    -- Replaces all virtual registers with the allocated locations.
    . fmap (replaceVirtD dvm) . fmap (replaceVirt vm)
    -- Handles preservation of registers and management of the stack.
    . pushPopRegs (regList L.\\ rs) (regListD L.\\ drs) n'' $ insts
    where
        -- Determines if an instruction is a dummy instruction.
        isDummy (Dummy _) = True
        isDummy _         = False
        -- Calculate statistics for each virtual register.
        stats = calcStats False 0 empty insts
        statsD = calcStats True 0 empty insts
        -- Group virtual registers into non-overlapping groups.
        vg = group . sortBy orderStats $ toList stats
        dvg = group . sortBy orderStats $ toList statsD
        -- Assign registers for the groups or spill them to the stack.
        (rs, vm, n) = assign regList stats 8 vg
        (drs, dvm, n') = assign regListD statsD n dvg
        -- Adjust the number of bytes allocated on the stack to be 16-byte aligned.
        n'' = if n' `mod` 16 == 0 then n' else n' - 8

-- | Statistics for a virtual register.
data Stat = Stat
    { start :: Integer -- ^ The index of the first instruction it appears.
    , end   :: Integer -- ^ The index of the last instruction is appears.
    , count :: Integer -- ^ The number of times the register is used.
    , time  :: Integer -- ^ The total lifetime of the register.
    }

-- | An ordering function for Stats besed on the start index.
orderStats :: (a, Stat) -> (a, Stat) -> Ordering
orderStats (_, s) (_, s') = start s `compare` start s'

-- | A map for looking up the statistics of a virtual register.
type StatMap = Map Integer Stat

-- | Calculates the statistics for each virtual register.
-- First argument is whether to calculate for SIMD or regular registers.
-- Second argument is the current instruction index.
calcStats :: Bool -> Integer -> StatMap -> [Inst] -> StatMap
calcStats _ _ sm [] = sm
calcStats d n sm (i:is) = calcStats d (n + 1) sm' is
    where
        vars = if d then extractVarsD i else extractVars i
        sm'  = P.foldl (bumpStats n) sm vars

-- | Extracts all regular virtual registers.
extractVars :: Tree t -> [Integer]
extractVars = \case
    Virt v -> [v]
    t -> composOpFold [] (++) extractVars t

-- | Extracts all SIMD virtual registers.
extractVarsD :: Tree t -> [Integer]
extractVarsD = \case
    VirtD v -> [v]
    t -> composOpFold [] (++) extractVarsD t

-- | Updates the statistics for the given register based on the instruction index.
-- First argument is the instruction index.
-- Third argument is the register number.
bumpStats :: Integer -> StatMap -> Integer -> StatMap
bumpStats n sm v
    | Just s <- M.lookup v sm
    = M.insert v s { end = n, count = count s + 1, time = n - start s + 1 } sm
    | otherwise
    = M.insert v Stat { start = n, end = n, count = 1, time = 1 } sm

-- | A map for looking up the group for a given register.
type GroupMap = Map Integer Integer

-- | A type synonym for a list-representation of a StatMap.
type StatList = [(Integer, Stat)]

-- | Assigns groups for each virtual register.
group :: StatList -> GroupMap
group = groups [1..] empty . divide

-- | The underlying function for 'group'.
-- First argument is a list of available groups.
group' :: [Integer] -> GroupMap -> StatList -> GroupMap
group' _      rm [] = rm
group' (r:rs) rm ss = groups rs rm' $ divide ss'
    where
        -- Select a register to assign to a group.
        v = pickVar (head ss) ss
        -- Remove the register from the register pool.
        ss' = P.filter ((/= v) . fst) ss
        -- Add the register to the group map.
        rm' = M.insert v r rm

-- | A helper function for 'group'' the handles isolated pools of registers.
groups :: [Integer] -> GroupMap -> [StatList] -> GroupMap
groups _  rm []  = rm
groups rs rm sss = unions $ fmap (group' rs rm) sss

-- | Selects a register to remove from the pool based on the lifetime of the variable.
pickVar :: (Integer, Stat) -> StatList -> Integer
pickVar (v, _) [] = v
pickVar (v, s) ((v', s') : ss)
    | time s' > time s = pickVar (v', s') ss
    | otherwise        = pickVar (v, s) ss

-- | Used by 'group' to divide a register pool into independant chunks.
divide :: StatList -> [StatList]
divide [] = []
divide ss = pre : divide post
    where
        (pre, post) = divide' (end . snd $ head ss) ss

-- | The underlying function for 'divide'.
-- First argument is the highest last instruction index encountered so far.
divide' :: Integer -> StatList -> (StatList, StatList)
divide' _ [s] = ([s], [])
divide' n ((v, s) : (v', s') : ss)
    | n < start s' && end s < start s'
    = ([(v, s)], (v', s') : ss)
    | otherwise
    = ((v, s) : pre, post)
    where (pre, post) = divide' (max n $ end s) $ (v', s') : ss

-- | A map for looking up the assigned location for a virtual register.
type VarMap = Map Integer Arg

-- | Assigns actual registers or stack space for each group.
-- Third argument is the current stack offset.
assign :: [Reg] -> StatMap -> Integer -> GroupMap -> ([Reg], VarMap, Integer)
assign rs sm n gm = (drop (length gs) rs, vm, n + fromIntegral sc * 8)
    where
        -- List all groups.
        gs = nub $ elems gm
        -- Calculate the number of groups that need to be spilled onto the stack.
        sc = max 0 $ length gs - length rs
        -- Calculate weights for each group.
        ws = calcWeights gs sm gm
        -- Sort the groups based on their weight.
        (_, gs') = unzip . sort $ zip ws gs
        -- Select the groups to be spilled.
        sgs = take sc gs'
        -- Select the groups to be assigned a register.
        rgs = drop sc gs'
        -- Make a mapping of groups to registers or stack addresses.
        gvm = fromList $ zip sgs (fmap (Address RBP . negate) [n,n+8..]) ++ zip rgs (fmap Register rs)
        -- Make a mapping of virtual registers to locations based on the group maps.
        vm = fmap (gvm !) gm

-- | Calculates the weight for each group based on use count and lifetime.
calcWeights :: [Integer] -> StatMap -> GroupMap -> [Rational]
calcWeights [] _ _ = []
calcWeights (g : gs) sm gm = product ws : calcWeights gs sm gm
    where
        -- List each variable belonging to this group.
        vs = keys $ M.filter (== g) gm
        -- Extract the statistics for each variable.
        ss = fmap (sm !) vs
        -- Calculate the weight part for each variable.
        ws = fmap (\s -> toRational (count s) / toRational (time s)) ss

-- | The list of registers that may be assigned to regular virtual registers.
regList :: [Reg]
regList = fmap R [15,14..12]

-- | The list of registers that may be assigned to SIMD virtual registers.
regListD :: [Reg]
regListD = fmap XMM [15,14..10]

-- | Replace each regular virtual register in a tree based on a VarMap.
replaceVirt :: VarMap -> Tree t -> Tree t
replaceVirt rm = \case
    Virt v -> rm ! v
    t -> composOp (replaceVirt rm) t

-- | Replace each SIMD virtual register in a tree based on a VarMap.
replaceVirtD :: VarMap -> Tree t -> Tree t
replaceVirtD rm = \case
    VirtD v -> rm ! v
    t -> composOp (replaceVirtD rm) t

-- | Makes sure that registers are preserved between function calls.
-- First argument is the regular registers to preserve.
-- Second argument is the SIMD registers to preserve.
-- Third argument is the number of local bytes to be allocated.
pushPopRegs :: [Reg] -> [Reg] -> Integer -> [Inst] -> [Inst]
pushPopRegs rs drs n = \case
    [] -> []
    Enter n' : is -> Enter (n + n') : (if even $ length rs + length drs then [] else [Push (Literal 0)])
        ++ fmap (Push . Register) rs ++ concatMap push drs ++ pushPopRegs rs drs n is
    Leave : is -> concatMap pop (reverse drs) ++ fmap (Pop . Register) (reverse rs)
        ++ Leave : pushPopRegs rs drs n is
    i : is -> i : pushPopRegs rs drs n is
    where
        push r = [Sub (Register RSP) $ Literal 8, MovSD (Address RSP 0) $ Register r]
        pop  r = [MovSD (Register r) $ Address RSP 0, Add (Register RSP) $ Literal 8]

-- | Replaces illegal instructions with equivalent legal instructions.
fix :: [Inst] -> [Inst]
fix = \case
    [] -> []
    Mov (Address dr doff) (Address sr soff) : is
        -> Mov temp (Address sr soff) : Mov (Address dr doff) temp : fix is
    MovSD (Address dr doff) (Address sr soff) : is
        -> MovSD tempD (Address sr soff) : MovSD (Address dr doff) tempD : fix is
    MovSD (Address dr doff) (Reference ref) : is
        -> MovSD tempD (Reference ref) : MovSD (Address dr doff) tempD : fix is
    Add (Address dr doff) (Address sr soff) : is
        -> Mov temp (Address sr soff) : Add (Address dr doff) temp : fix is
    AddSD (Address dr doff) (Address sr soff) : is
        -> MovSD tempD (Address dr doff) : AddSD tempD (Address sr soff)
            : MovSD (Address dr doff) tempD : fix is
    AddSD (Address dr doff) (Reference ref) : is
        -> MovSD tempD (Address dr doff) : AddSD tempD (Reference ref)
            : MovSD (Address dr doff) tempD : fix is
    Sub (Address dr doff) (Address sr soff) : is
        -> Mov temp (Address sr soff) : Sub (Address dr doff) temp : fix is
    SubSD (Address dr doff) (Address sr soff) : is
        -> MovSD tempD (Address dr doff) : SubSD tempD (Address sr soff)
            : MovSD (Address dr doff) tempD : fix is
    SubSD (Address dr doff) (Reference ref) : is
        -> MovSD tempD (Address dr doff) : SubSD tempD (Reference ref)
            : MovSD (Address dr doff) tempD : fix is
    IMul (Address dr doff) src : is
        -> Mov temp (Address dr doff) : IMul temp src
            : Mov (Address dr doff) temp : fix is
    MulSD (Address dr doff) (Address sr soff) : is
        -> MovSD tempD (Address dr doff) : MulSD tempD (Address sr soff)
            : MovSD (Address dr doff) tempD : fix is
    MulSD (Address dr doff) (Reference ref) : is
        -> MovSD tempD (Address dr doff) : MulSD tempD (Reference ref)
            : MovSD (Address dr doff) tempD : fix is
    DivSD (Address dr doff) (Address sr soff) : is
        -> MovSD tempD (Address dr doff) : DivSD tempD (Address sr soff)
            : MovSD (Address dr doff) tempD : fix is
    DivSD (Address dr doff) (Reference ref) : is
        -> MovSD tempD (Address dr doff) : DivSD tempD (Reference ref)
            : MovSD (Address dr doff) tempD : fix is
    CMP (Address dr doff) (Address sr soff) : is
        -> Mov temp (Address sr soff) : CMP (Address dr doff) temp : fix is
    COMISD (Address dr doff) (Address sr soff) : is
        -> MovSD tempD (Address dr doff) : COMISD tempD (Address sr soff) : fix is
    COMISD (Address dr doff) (Reference ref) : is
        -> MovSD tempD (Address dr doff) : COMISD tempD (Reference ref) : fix is
    i : is -> i : fix is

-- | The regular register assigned as temporary.
temp :: Arg
temp = Register $ R 11

-- | The SIMD register assigned as temporary.
tempD :: Arg
tempD = Register $ XMM 9