module Backend.X86.Compile (compile) where

import Prelude as P

import Control.Eff
import Control.Eff.State.Lazy
import Control.Eff.Writer.Lazy
import Control.Monad

import Data.List as L
import Data.Map as M
import Data.Void

import System.Info

import BNFC.AbsJavalike as J
import BNFC.AbsX as X

import Utility.Effects
import Utility.Javalike

-- | Compiles a javalike program into x86 code.
-- Requires register allocation before printing.
compile :: J.Prog -> X.Prog
compile (J.Program defs)
    = X.Program (external ++ decls)
    . (strData :) . (dData :)
    . runProgEffs strMap dMap
    $ mapM compTopDef defs
    where
        decls = fmap (\(FnDef _ name _ _) -> Global $ toFnName name) defs
        (strMap, strData) = extractStrings $ J.Program defs
        (dMap, dData) = extractDoubles $ J.Program defs

-- | Extracts all strings from the program.
-- Returns a map of strings to idents and a data section with the string data.
extractStrings :: J.Prog -> (StrMap, Sect)
extractStrings prog = (strMap, strData)
    where
        strData = Data . fmap (uncurry $ flip DB) . toList $ strMap
        strMap  = fromList . zip strs . fmap (X.Ident . ('s':) . show @Int) $ [0..]
        strs    = nub . composOpMonoid extract $ prog
        extract :: J.Tree t -> [String]
        extract = \case
            EString s -> [s]
            t -> composOpMonoid extract t

-- | Extracts all double constants from the program.
-- Returns a map of doubles to idents and a data section with the constants.
extractDoubles :: J.Prog -> (DoubMap, Sect)
extractDoubles prog = (dMap, dData)
    where
        dData = Data . fmap (uncurry $ flip DQ) . toList $ dMap
        dMap  = fromList . zip ds . fmap (X.Ident . ('d':) . show @Int) $ [0..]
        ds    = nub . (0 :) . composOpMonoid extract $ prog
        extract :: J.Tree t -> [Double]
        extract = \case
            ELitDoub d -> [d]
            t -> composOpMonoid extract t

-- | A map for looking up double constant names.
type DoubMap = Map Double X.Ident

-- | A map for looking up string constant names.
type StrMap = Map String X.Ident

-- | The extensible effects relevant during the whole compilation.
type ProgEffs r = (Member (State CMap) r, Member (State StrMap) r, Member (State DoubMap) r)

-- | Function for running the program effects taking the string and double maps.
runProgEffs :: StrMap -> DoubMap -> Eff (State DoubMap :> State StrMap :> State CMap :> Void) a -> a
runProgEffs strMap dMap = run . evalState empty . evalState strMap . evalState dMap

-- | The extensible effects relevant during compilation of a function.
type FnEffs r = (ProgEffs r, Member (State X.Ident) r, Member (State VarMap) r, Member (Writer Inst) r)

-- | Function for running the function effects taking a variable map populated with the function arguments.
runFnEffs :: ProgEffs r => X.Ident -> VarMap -> Eff (State X.Ident :> State VarMap :> Writer Inst :> r) () -> Eff r [Inst]
runFnEffs ret vMap = fmap fst . runWriter (:) [] . runState vMap . runState ret

-- | Compiles a top definition.
compTopDef :: ProgEffs r => TopDef -> Eff r Sect
compTopDef (FnDef _ name args blk) = do
    ret <- newLabel
    let pre  = [Label $ toFnName name, Enter 0]
        post = [Label ret, Leave, X.Ret]
        vMap = gatherArgs args
    Text . (pre ++) . (++ post) <$> runFnEffs ret vMap (compBlk blk)

-- | Creates a variable map containing mappings for each parameter.
gatherArgs :: [J.Arg] -> VarMap
gatherArgs args = gather args argRegs argRegsD 8 empty
    where
        gather :: [J.Arg] -> [Reg] -> [Reg] -> Integer -> VarMap -> VarMap
        gather [] _ _ _ vMap = vMap
        gather (Argument Doub name : as) rs [] n vMap = let n' = n + 8
            in gather as rs [] n' $ M.insert name (Address RBP n') vMap
        gather (Argument Doub name : as) rs (r : drs) n vMap
            = gather as rs drs n $ M.insert name (Register r) vMap
        gather (Argument _ name : as) [] drs n vMap = let n' = n + 8
            in gather as [] drs n' $ M.insert name (Address RBP n') vMap
        gather (Argument _ name : as) (r : rs) drs n vMap
            = gather as rs drs n $ M.insert name (Register r) vMap

-- | Compiles a block.
compBlk :: FnEffs r => Blk -> Eff r ()
compBlk (Block stmts) = mapM_ compStmt stmts

-- | Compiles a statement.
compStmt :: FnEffs r => Stmt -> Eff r ()
compStmt = \case
    BStmt blk -> compBlk blk
    Empty -> return ()
    Decl t items -> forM_ items $ \case
        NoInit name -> do
            var <- newVar t
            putVar name var
            case t of
                Doub -> do
                    ref <- getDouble 0
                    tell . mov t var $ Reference ref
                _ -> tell . mov t var $ Literal 0
        Init name expr -> do
            val <- compExpr expr
            var <- newVar t
            putVar name var
            tell $ mov t var val
    Ass (LTyped t ex) expr -> do
        val <- compExpr expr
        case ex of
            Var name -> do
                var <- getVar name
                tell $ mov t var val
            Indexed name idxs -> do
                -- The type is not important
                let r = tempReg Void
                arr <- getVar name
                ptr <- getElementPointer arr idxs
                tell $ Mov (Register r) ptr
                case t of
                    Doub -> tell $ MovSD (Address r 0) val
                    _    -> tell $ Mov   (Address r 0) val
            _ -> error $ "INTERNAL ERROR: x86 compiler can not handle the left hand side expression " ++ show ex
    J.Ret expr -> do
        let t = typeOf expr
        val <- compExpr expr
        tell $ mov t (Register $ retReg t) val
        ret <- get
        tell $ Jump None ret
    VRet -> do
        ret <- get
        tell $ Jump None ret
    Incr name -> getVar name >>= tell . Inc
    Decr name -> getVar name >>= tell . Dec
    Cond expr stmt -> do
        end <- newLabel
        compBool end expr
        compStmt stmt
        tell $ Label end
    CondElse expr stmtT stmtF -> do
        false <- newLabel
        end   <- newLabel
        compBool false expr
        compStmt stmtT
        tell $ Jump None end
        tell $ Label false
        compStmt stmtF
        tell $ Label end
    While expr stmt -> do
        body  <- newLabel
        check <- newLabel
        end   <- newLabel
        tell $ Jump None check
        tell $ Label body
        compStmt stmt
        -- The check is placed after the body so that register allocation does not break the program.
        tell $ Label check
        compBool end expr
        -- Adding dummy instructions to prevent variable collision during register allocation.
        makeVarDummies stmt
        tell $ Jump None body
        tell $ Label end
    SExp expr -> void $ compExpr expr
    For t name expr stmt -> rewriteFor t name expr stmt <$> newJVar <*> newJVar <*> newJVar >>= compBlk

-- | Creates dummy instructions for all variables used but not declared in the statement.
makeVarDummies :: FnEffs r => Stmt -> Eff r ()
makeVarDummies stmt = flip mapM_ vars $ \name -> do
    var <- getVar name
    tell $ Dummy var
    where
        vars = nub (getUsedVars stmt) L.\\ nub (getDeclVars stmt)
        getUsedVars :: J.Tree t -> [J.Ident]
        getUsedVars = \case
            Var name -> [name]
            Indexed name idxs -> name : concatMap getUsedVars idxs
            t -> composOpMonoid getUsedVars t
        getDeclVars :: J.Tree t -> [J.Ident]
        getDeclVars = \case
            Init name _ -> [name]
            NoInit name -> [name]
            t -> composOpMonoid getDeclVars t

-- | Compiles expressions.
compExpr :: FnEffs r => Expr -> Eff r X.Arg
-- Boolean expressions need special considerations.
compExpr (Typed Bool ex) = case ex of
    ELitTrue  -> return $ Literal 1
    ELitFalse -> return $ Literal 0
    EApp name exprs -> compApp Bool name exprs
    LExpr (LTyped _ (Var name)) -> getVar name
    LExpr (LTyped _ (Indexed name idxs)) -> getIndexed Bool name idxs
    -- Other boolean expressions get the value based on compBool.
    _ -> do
        end <- newLabel
        var <- newVar Bool
        tell . Mov var $ Literal 0
        compBool end $ Typed Bool ex
        tell . Mov var $ Literal 1
        tell $ Label end
        return var
-- Non-boolean expressions.
compExpr (Typed t ex) = case ex of
    ELitInt  i -> return $ Literal i
    ELitDoub d -> Reference <$> getDouble d
    EString  s -> Reference <$> getString s
    EApp name exprs -> compApp t name exprs
    Length expr -> do
        -- The type is not important
        let r = tempReg Void
        arr <- compExpr expr
        var <- newVar Int
        tell $ Mov (Register r) arr
        tell . Mov var $ Address r 0
        return var
    LExpr (LTyped _ ex') -> case ex' of
        Var name -> getVar name
        Indexed name idxs -> getIndexed t name idxs
        _ -> error $ "INTERNAL ERROR: x86 compiler can not handle the expression " ++ show ex'
    J.Neg expr -> do
        val <- compExpr expr
        var <- newVar t
        zero t >>= tell . mov t var
        tell $ sub t var val
        return var
    EAdd exprL op exprR -> do
        valL <- compExpr exprL
        valR <- compExpr exprR
        var <- newVar t
        tell $ mov t var valL
        let inst = case op of
                Plus  -> add t
                Minus -> sub t
        tell $ inst var valR
        return var
    EMul exprL op exprR -> do
        valL <- compExpr exprL
        valR <- compExpr exprR
        var <- newVar t
        case op of
            Times -> do
                tell $ mov t var valL
                tell $ mul t var valR
            _ -> case t of
                Doub -> do
                    tell $ mov t var valL
                    tell $ DivSD var valR
                _ -> do
                    tell $ mov t (Register RAX) valL
                    tell CQO
                    tell $ mov t var valR
                    tell $ IDiv var
                    tell . mov t var . Register $ case op of
                        Div -> RAX
                        Mod -> RDX
        return var
    New t' idxs -> compNew t' idxs
    _ -> error $ "INTERNAL ERROR: x86 compiler can not handle the expression " ++ show ex

-- | Compiles a boolean expression as control flow.
-- Jumps to the given label if the expression is false, otherwise falls through.
compBool :: FnEffs r => X.Ident -> Expr -> Eff r ()
compBool lblF (Typed Bool ex) = case ex of
    ELitTrue -> return ()
    ELitFalse -> tell $ Jump None lblF
    EApp name exprs -> do
        val <- compApp Bool name exprs
        tell . CMP val $ Literal 0
        tell $ Jump E lblF
    LExpr (LTyped _ ex') -> case ex' of
        Var name -> do
            var <- getVar name
            tell . CMP var $ Literal 0
            tell $ Jump E lblF
        Indexed name idxs -> do
            val <- getIndexed Bool name idxs
            tell . CMP val $ Literal 0
            tell $ Jump E lblF
        _ -> error $ "INTERNAL ERROR: x86 compiler can not handle the expression " ++ show ex'
    Not expr -> do
        false <- newLabel
        compBool false expr
        tell $ Jump None lblF
        tell $ Label false
    ERel exprL op exprR -> do
        let t = typeOf exprL
        valL <- compExpr exprL
        valR <- compExpr exprR
        if isReg valL
            then tell $ cmp t valL valR
            else do
                temp <- newVar t
                tell $ mov t temp valL
                tell $ cmp t temp valR
        let cond = case op of
                EQU  -> X.NE
                J.NE -> E
                GTH  -> if t == Doub then BE else X.LE
                J.GE -> if t == Doub then B  else L
                LTH  -> if t == Doub then AE else X.GE
                J.LE -> if t == Doub then A  else G
        tell $ Jump cond lblF
    EAnd exprL exprR -> do
        compBool lblF exprL
        compBool lblF exprR
    EOr exprL exprR -> do
        right <- newLabel
        end <- newLabel
        compBool right exprL
        tell $ Jump None end
        tell $ Label right
        compBool lblF exprR
        tell $ Label end
    _ -> error $ "INTERNAL ERROR: x86 compiler can not handle the bool expression " ++ show ex
compBool _ t = error $ "INTERNAL ERROR: trying to compile non-bool expression as bool " ++ show t

-- | Compiles a new-expression.
compNew :: FnEffs r => Type -> [Idx] -> Eff r X.Arg
compNew _ [] = error "INTERNAL ERROR: zero-dimensional indexing not allowed"
compNew t [Index expr] = do
    ptr <- newVar $ Array t
    num <- compExpr expr
    tell $ Mov (Register RDI) num
    tell . Inc $ Register RDI
    tell . IMul (Register RDI) $ Literal 8
    tell . Call . toFnName $ J.Ident "new"
    tell $ Mov (Address RAX 0) num
    tell . Mov ptr $ Register RAX
    return ptr
compNew t idxs = do
    name <- newJVar
    nestedNew name t idxs <$> newJVar <*> newJVar >>= compBlk
    getVar name

-- | Returns the pointer to an element in an array given by the indicies.
getElementPointer :: FnEffs r => X.Arg -> [Idx] -> Eff r X.Arg
getElementPointer _ [] = error "INTERNAL ERROR: zero-dimensional indexing not allowed"
getElementPointer arr [Index expr] = do
    -- The types are not important
    let r = Register $ tempReg Void
    ptr <- newVar Void
    idx <- compExpr expr
    tell $ Mov r idx
    tell $ Inc r
    tell . IMul r $ Literal 8
    tell $ Add r arr
    tell $ Mov ptr r
    return ptr
getElementPointer arr (idx : idxs) = do
    -- The types are not important
    let r = tempReg Void
    arr' <- newVar Void
    ptr <- getElementPointer arr [idx]
    tell $ Mov (Register r) ptr
    tell . Mov arr' $ Address r 0
    getElementPointer arr' idxs

-- | Compiles an array indexing expression.
getIndexed :: FnEffs r => Type -> J.Ident -> [Idx] -> Eff r X.Arg
getIndexed t name idxs = do
    -- The type is not important
    let r = tempReg Void
    var <- newVar t
    arr <- getVar name
    ptr <- getElementPointer arr idxs
    tell $ Mov (Register r) ptr
    case t of
        Doub -> tell . MovSD var $ Address r 0
        _    -> tell . Mov   var $ Address r 0
    return var

-- | Compiles a function call.
compApp :: FnEffs r => Type -> J.Ident -> [Expr] -> Eff r X.Arg
compApp t name exprs = do
    vals <- mapM compExpr exprs
    let (n, insts) = prepareArgs $ zip (fmap typeOf exprs) vals
    -- Make sure the stack is 16-byte aligned.
    unless (n `mod` 16 == 0) . tell . Push $ Literal 0
    -- Compile argument assignments in reverse order (due to stack arguments).
    mapM_ (mapM_ compArgInst) $ reverse insts
    tell . Call $ toFnName name
    -- Deallocate the arguments from the stack.
    unless (n == 0) . tell . Add (Register RSP) . Literal $ if n `mod` 16 == 0 then n else n + 8
    -- Make a new variable for the return that will be ignored if the return is void.
    var <- newVar t
    unless (t == Void) . tell . mov t var . Register $ retReg t
    return var

-- | Compiles argument instructions, fixing illegal instructions.
compArgInst :: FnEffs r => Inst -> Eff r ()
compArgInst = \case
    MovSD (Address dr doff) (Reference ref) -> do
        var <- newVar Doub
        tell . MovSD var $ Reference ref
        tell $ MovSD (Address dr doff) var
    inst -> tell inst

-- | Generates the instructions required for argument assignments.
prepareArgs :: [(Type, X.Arg)] -> (Integer, [[Inst]])
prepareArgs args = prepare args argRegs argRegsD 0
    where
        prepare :: [(Type, X.Arg)] -> [Reg] -> [Reg] -> Integer -> (Integer, [[Inst]])
        prepare [] _ _ n = (n, [])
        prepare ((Doub, v) : as) rs [] n = let (n', is) = prepare as rs [] $ n + 8
            in (n', [Sub (Register RSP) $ Literal 8, MovSD (Address RSP 0) v] : is)
        prepare ((Doub, v) : as) rs (r : drs) n = let (n', is) = prepare as rs drs n
            in (n', [MovSD (Register r) v] : is)
        prepare ((_, v) : as) [] drs n = let (n', is) = prepare as [] drs $ n + 8
            in (n', [Push v] : is)
        prepare ((t, v) : as) (r : rs) drs n = let (n', is) = prepare as rs drs n
            in (n', [mov t (Register r) v] : is)

-- | The built-in function declarations.
external :: [Decl]
external = fmap (Extern . toFnName . J.Ident)
    ["printInt", "printDouble", "printString", "readInt", "readDouble", "new"]

-- | Formatting the function names for x86.
toFnName :: J.Ident -> X.Ident
toFnName (J.Ident name) = case os of
    "linux"  -> X.Ident name
    "darwin" -> X.Ident $ '_' : name
    _        -> error "INTERNAL ERROR: operative system not supported"

-- | A map containing the location of a given variable.
type VarMap = Map J.Ident X.Arg

-- | Stores the location of a variable.
putVar :: Member (State VarMap) r => J.Ident -> X.Arg -> Eff r ()
putVar name val = modify $ M.insert name val

-- | Gets the location of a variable.
getVar :: Member (State VarMap) r => J.Ident -> Eff r X.Arg
getVar name = (! name) <$> get

-- | Returns a new location for a variable of the given type.
newVar :: Member (State CMap) r => Type -> Eff r X.Arg
newVar Doub = VirtD <$> new "virtd"
newVar _    = Virt  <$> new "virt"

-- | Returns a new label.
newLabel :: Member (State CMap) r => Eff r X.Ident
newLabel = X.Ident <$> newPrefix "l" "label"

-- | Returns a new temporary javalike variable.
newJVar :: Member (State CMap) r => Eff r J.Ident
newJVar = J.Ident <$> newPrefix "t" "temp"

-- | Returns the ident of a given string.
getString :: Member (State StrMap) r => String -> Eff r X.Ident
getString s = (! s) <$> get

-- | Returns the ident of a given double.
getDouble :: Member (State DoubMap) r => Double -> Eff r X.Ident
getDouble d = (! d) <$> get

-- | The list of registers for regular function arguments.
argRegs :: [Reg]
argRegs = [RDI, RSI, RDX, RCX, R 8, R 9]

-- | The list of registers for SIMD function arguments.
argRegsD :: [Reg]
argRegsD = fmap XMM [0..7]

-- | The return register for the given type.
retReg :: Type -> Reg
retReg Doub = XMM 0
retReg _    = RAX

-- | The register assigned as temporary.
tempReg :: Type -> Reg
tempReg Doub = XMM 8
tempReg _    = R 10

-- | The zero constant for the given type.
zero :: Member (State DoubMap) r => Type -> Eff r X.Arg
zero Doub = Reference <$> getDouble 0
zero _    = return $ Literal 0

-- | Selects the appropriate mov instruction.
mov :: Type -> X.Arg -> X.Arg -> Inst
mov Doub = MovSD
mov Str  = LEA
mov _    = Mov

-- | Selects the appropriate mov instruction.
cmp :: Type -> X.Arg -> X.Arg -> Inst
cmp Doub = COMISD
cmp _    = CMP

-- | Selects the appropriate add instruction.
add :: Type -> X.Arg -> X.Arg -> Inst
add Doub = AddSD
add _    = Add

-- | Selects the appropriate sub instruction.
sub :: Type -> X.Arg -> X.Arg -> Inst
sub Doub = SubSD
sub _    = Sub

-- | Selects the appropriate mul instruction.
mul :: Type -> X.Arg -> X.Arg -> Inst
mul Doub = MulSD
mul _    = IMul

-- | Determines whether an argument is (likely to be) a register.
isReg :: X.Arg -> Bool
isReg (Register _) = True
isReg (Virt     _) = True
isReg (VirtD    _) = True
isReg _            = False