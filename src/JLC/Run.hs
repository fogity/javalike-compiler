module JLC.Run where

import System.Exit
import System.FilePath
import System.Info
import System.IO
import System.Process

import BNFC.ErrM
import BNFC.LexJavalike
import BNFC.ParJavalike

import Backend.LLVM.Compile as L
import Backend.Normalize
import Backend.X86.Allocate
import Backend.X86.Compile as X
import Backend.X86.Optimize
import Backend.X86.Print

import Frontend.TypeCheck

-- | Available backends.
data Backend = LLVM | X86

-- | Each file is read, parsed, type checked and compiled.
-- The result of parsing and type checking is printed to standard error.
-- The compiled file is written to the same location as the input file.
processFile :: FilePath -> Backend -> FilePath -> IO ()
processFile lib backend file = do
    source <- readFile file
    case pProg (tokens source) >>= typeCheck of
        Bad s -> do
            hPutStrLn stderr "ERROR"
            hPutStrLn stderr s
            exitFailure
        Ok t -> do
            hPutStrLn stderr "OK"
            let t' = normalize t
            case backend of
                LLVM -> do
                    writeFile llFile $ L.compile t'
                    callCommand $ "llvm-as " ++ llFile
                    callCommand $ "llvm-link " ++ bcFile ++ " " ++ lib </> "runtime.bc"
                        ++ " | opt | llc -filetype=obj > " ++ oFile
                    callCommand $ "gcc " ++ oFile ++ " -o " ++ rawFile
                X86 -> do
                    -- The optimization passes are run before and after allocation for greater optimization.
                    writeFile sFile . printX86 . optimize . allocate . optimize . X.compile $ t'
                    let format = case os of
                            "linux"  -> "elf64"
                            "darwin" -> "macho64"
                            _        -> error "INTERNAL ERROR: operative system not supported"
                    callCommand $ "nasm -f " ++ format ++ " " ++ sFile ++ " -o " ++ oFile
                    callCommand $ "gcc " ++ oFile ++ " " ++ lib </> "runtime.o" ++ " -o " ++ rawFile
            exitSuccess
    where
        rawFile = dropExtension file
        llFile  = rawFile <.> "ll"
        bcFile  = rawFile <.> "bc"
        sFile   = rawFile <.> "s"
        oFile   = rawFile <.> "o"
        