module Frontend.TypeCheck (typeCheck) where

import Prelude hiding (lookup)

import Control.Monad.State

import Data.Map

import Lens.Micro.Platform

import BNFC.AbsJavalike
import BNFC.ErrM
import BNFC.PrintJavalike

import Utility.Functions
import Utility.Javalike

-- | A type synonym for the function type.
type FnType = (Type, [Type])

-- | A type synonym for the function type map.
type FnMap = Map Ident FnType

-- | A type synonym for the scope.
type Scope = Map Ident Type

-- | A type synonym for the environment.
type Env = [Scope]

-- | The state for type checking.
data TCS = TCS
    { _fnMap  :: FnMap -- ^ A map containing the type information for all functions.
    , _fnName :: Ident -- ^ The name of the currently checked function.
    , _rType  :: Type  -- ^ The return type of the currently checked function.
    , _env    :: Env   -- ^ The current environment.
    , _dType  :: Type  -- ^ The type of the current variable declaration.
    }

-- Makes lenses for the state.
makeLenses ''TCS

-- | The monad used for type checking. It is the 'Err' monad transformed into a state monad with state 'TCS'.
type TC = StateT TCS Err

-- | The main type checking function. Runs in the 'Err' monad.
typeCheck :: Prog -> Err Prog
typeCheck = flip evalStateT emptyTCS . check

-- | A dummy state for initialising type checking.
emptyTCS :: TCS
emptyTCS = TCS {_fnMap = empty, _fnName = Ident "", _rType = Void, _env = [], _dType = Void}

-- | Type checks parts of a syntax tree.
check :: Tree t -> TC (Tree t)
check = \case
    -- Prog
    Program defs -> do
        fnMap <~ buildFnMap defs
        checkMain
        Program <$> mapM check defs
    -- TopDef
    FnDef rt name args blk -> do
        fnName .= name
        rType  .= rt
        env    .= [empty]
        unless (rt == Void) $ checkReturn blk
        mapM_ (\(Argument t n) -> addVar t n) args
        FnDef rt name args <$> check blk
    -- Blk
    Block stmts -> Block <$> inNewScope (mapM check stmts)
    -- Stmt
    Empty -> return Empty
    BStmt blk -> BStmt <$> check blk
    Decl t items -> do
        dType .= t
        Decl t <$> mapM check items
    Ass lexpr expr -> do
        lexpr' <- check lexpr
        expr' <- check expr
        let lt = typeOfL lexpr'
            et = typeOf expr'
        unless (et == lt) $ typeErrorFnE et lt "assignment"
        return $ Ass lexpr' expr'
    Incr name -> do
        t <- getType name
        unless (t == Int) . typeErrorFn $ "incrementing non-int variable '" ++ printTree name ++ "'"
        return $ Incr name
    Decr name -> do
        t <- getType name
        unless (t == Int) . typeErrorFn $ "decrementing non-int variable '" ++ printTree name ++ "'"
        return $ Decr name
    Ret expr -> do
        expr' <- check expr
        let et = typeOf expr'
        rt <- use rType
        when (rt == Void) $ typeErrorFn "non-void return in void function"
        unless (et == rt) $ typeErrorFnE et rt "return statement"
        return $ Ret expr'
    VRet -> do
        rt <- use rType
        unless (rt == Void) $ typeErrorFn "void return in non-void function"
        return VRet
    Cond expr stmt -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Bool) $ typeErrorFnE et Bool "if statement"
        Cond expr' <$> inNewScope (check stmt)
    CondElse expr stmtT stmtF -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Bool) $ typeErrorFnE et Bool "if statement"
        CondElse expr' <$> inNewScope (check stmtT) <*> inNewScope (check stmtF)
    While expr stmt -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Bool) $ typeErrorFnE et Bool "while statement"
        While expr' <$> inNewScope (check stmt)
    SExp expr -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Void) $ typeErrorFnE et Void "expression statement"
        return $ SExp expr'
    For t name expr stmt -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Array t) $ typeErrorFnE et (Array t) "for statement"
        For t name expr' <$> inNewScope (addVar t name >> check stmt)
    -- Item
    NoInit name -> do
        t <- use dType
        addVar t name
        return $ NoInit name
    Init name expr -> do
        expr' <- check expr
        let et = typeOf expr'
        t <- use dType
        unless (et == t) . typeErrorFnE et t $ "declaration of '" ++ printTree name ++ "'"
        addVar t name
        return $ Init name expr'
    -- Idx
    Index expr -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Int) $ typeErrorFnE et Int "array index"
        return $ Index expr'
    -- LExpr
    Var name -> flip LTyped (Var name) <$> getType name
    Indexed name idxs -> do
        t <- getType name
        unless (dim t >= length idxs) . typeErrorFn $ "trying to index a scalar"
        LTyped (array (element t) $ dim t - length idxs) . Indexed name <$> mapM check idxs
    -- Expr
    ELitInt  i -> return . Typed Int  $ ELitInt  i
    ELitDoub d -> return . Typed Doub $ ELitDoub d
    ELitTrue   -> return . Typed Bool $ ELitTrue
    ELitFalse  -> return . Typed Bool $ ELitFalse
    EApp name exprs -> use (fnMap . at name) >>= \case
        Nothing -> staticErrorFn $ "undeclared function '" ++ printTree name ++ "'"
        Just (rt, types) -> do
            -- Check each argument.
            (ets, exprs') <- unzip . fmap (\expr -> (typeOf expr, expr)) <$> mapM check exprs
            -- Check the number of arguments.
            unless (length ets == length types) . staticErrorFn
                $ "wrong number of arguments to '" ++ printTree name ++ "'"
            -- Check the type of each argument.
            forM_ (zip3 @Int [1..] ets types) $ \(n, et, t)
                -> unless (et == t) . typeErrorFnE et t
                    $ "argument " ++ show n ++ " to '" ++ printTree name ++ "'"
            return . Typed rt $ EApp name exprs'
    EString s -> return . Typed Str $ EString s
    Length expr -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (isArray et) . typeErrorFn
            $ "found " ++ printTree et ++ " but expected an array when accessing the length"
        return . Typed Int $ Length expr'
    LExpr lexpr -> do
        lexpr' <- check lexpr
        return . Typed (typeOfL lexpr') $ LExpr lexpr'
    Neg expr -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et `elem` [Int, Doub]) . typeErrorFn
            $ "found " ++ printTree et ++ " but expected number in negation"
        return . Typed et $ Neg expr'
    Not expr -> do
        expr' <- check expr
        let et = typeOf expr'
        unless (et == Bool) . typeErrorFnE et Bool $ "logical negation"
        return . Typed et $ Not expr'
    EMul exprL op exprR -> do
        exprL' <- check exprL
        exprR' <- check exprR
        let etL = typeOf exprL'
            etR = typeOf exprR'
        unless (etL `elem` [Int, Doub]) . typeErrorFn
            $ "found " ++ printTree etL ++ " but expected number in " ++ printTree op
        when (op == Mod && etL /= Int) $ typeErrorFnE etL Int $ "%"
        unless (etL == etR) . typeErrorFnE etL etR $ printTree op
        return . Typed etL $ EMul exprL' op exprR'
    EAdd exprL op exprR -> do
        exprL' <- check exprL
        exprR' <- check exprR
        let etL = typeOf exprL'
            etR = typeOf exprR'
        unless (etL `elem` [Int, Doub]) . typeErrorFn
            $ "found " ++ printTree etL ++ " but expected number in " ++ printTree op
        unless (etL == etR) . typeErrorFnE etL etR $ printTree op
        return . Typed etL $ EAdd exprL' op exprR'
    ERel exprL op exprR -> do
        exprL' <- check exprL
        exprR' <- check exprR
        let etL = typeOf exprL'
            etR = typeOf exprR'
        unless (etL `elem` [Int, Doub, Bool]) . typeErrorFn
            $ "found " ++ printTree etL ++ " but expected number in " ++ printTree op
        when (etL == Bool && op `notElem` [EQU, NE]) $ typeErrorFnE etL Int $ printTree op
        unless (etL == etR) . typeErrorFnE etL etR $ printTree op
        return . Typed Bool $ ERel exprL' op exprR'
    EAnd exprL exprR -> do
        exprL' <- check exprL
        exprR' <- check exprR
        let etL = typeOf exprL'
            etR = typeOf exprR'
        unless (etL == Bool) $ typeErrorFnE etL Bool "conjunction"
        unless (etR == Bool) $ typeErrorFnE etL Bool "conjunction"
        return . Typed etL $ EAnd exprL' exprR'
    EOr exprL exprR -> do
        exprL' <- check exprL
        exprR' <- check exprR
        let etL = typeOf exprL'
            etR = typeOf exprR'
        unless (etL == Bool) $ typeErrorFnE etL Bool "disjunction"
        unless (etR == Bool) $ typeErrorFnE etL Bool "disjunction"
        return . Typed etL $ EOr exprL' exprR'
    New t idxs -> Typed (array t $ length idxs) . New t <$> mapM check idxs
    -- Otherwise
    t -> error $ "INTERNAL ERROR: type checker does not handle " ++ show t

-- | Builds the function type map. Checks for duplicate function names.
buildFnMap :: [TopDef] -> TC FnMap
buildFnMap [] = return builtInFnMap
buildFnMap (FnDef rt name args _ : defs) = do
    fm <- buildFnMap defs
    when (name `member` fm) . staticError $ "duplicate definition of function '" ++ printTree name ++ "'"
    return $ insert name (rt, fmap (\(Argument t _) -> t) args) fm

-- | A function type map containing the primitive functions.
builtInFnMap :: FnMap
builtInFnMap = fromList
    [ (Ident "printInt"    , (Void , [Int  ]))
    , (Ident "printDouble" , (Void , [Doub ]))
    , (Ident "printString" , (Void , [Str  ]))
    , (Ident "readInt"     , (Int  , [     ]))
    , (Ident "readDouble"  , (Doub , [     ]))
    ]

-- | Adds a new scope only for the enclosed type check.
inNewScope :: TC a -> TC a
inNewScope action = do
    env %= (empty:)
    ret <- action
    env %= tail
    return ret

-- | A function to add a variable into the current scope. Checks for duplicate variable names.
addVar :: Type -> Ident -> TC ()
addVar t name = do
    when (t == Void) . typeErrorFn $ "the variable '" ++ printTree name ++ "' is declared as void"
    when (element t == Void) . typeErrorFn $ "the variable '" ++ printTree name ++ "' is declared as a void array"
    scope <- use $ env . _head
    when (name `member` scope) . staticErrorFn $ "the variable '" ++ printTree name ++ "' is declared twice"
    env . _head %= insert name t

-- | A function to get the type of a variable in the environment. Checks for undeclared variables.
getType :: Ident -> TC Type
getType name = findDeep (lookup name) <$> use env >>= \case
    Nothing -> staticErrorFn $ "undeclared variable '" ++ printTree name ++ "'"
    Just t  -> return t

-- | Checks that the main function exists and that it has the correct type.
checkMain :: TC ()
checkMain = use (fnMap . at (Ident "main")) >>= \case
    Just (Int, []) -> return ()
    Nothing        -> staticError "missing main function"
    _              -> staticError "main function has the wrong type"

-- | Checks that a function is guaranteed to return.
checkReturn :: Blk -> TC ()
checkReturn blk = unless (doesReturn blk) $ staticErrorFn "missing return"

-- | A function to report a static error.
staticError :: String -> TC a
staticError = lift . Bad . ("STATIC ERROR: " ++)

-- | A function to report a type error.
typeError :: String -> TC a
typeError = lift . Bad . ("TYPE ERROR: " ++)

-- | A version of 'staticError' that appends the function name.
staticErrorFn :: String -> TC a
staticErrorFn s = do
    name <- use fnName
    staticError $ s ++ " in function '" ++ printTree name ++ "'"

-- | A version of 'typeError' that appends the function name.
typeErrorFn :: String -> TC a
typeErrorFn s = do
    name <- use fnName
    typeError $ s ++ " in function '" ++ printTree name ++ "'"

-- | A version of 'typeErrorFn' that prepends the type information.
typeErrorFnE :: Type -> Type -> String -> TC a
typeErrorFnE f e = typeErrorFn . (("found " ++ printTree f ++ " but expected " ++ printTree e ++ " in ") ++)